#!/bin/bash

set -x
set -e

cp root/usr/local/bin/xSwapOff                /usr/local/bin/xSwapOff

chmod +x /usr/local/bin/xSwapOff

cp root/usr/local/bin/xSwapOn                 /usr/local/bin/xSwapOn

chmod +x /usr/local/bin/xSwapOn

cp root/etc/systemd/system/xSwap.service      /etc/systemd/system/xSwap.service

systemctl enable xSwap.service


